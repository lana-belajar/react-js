# Mengenal React 

React adalah library FrontEnd yang diciptakan oleh salah satu karyawan Facebook, yaitu : Jordan Walke. Banyak yang menyebut ReactJS sebagai Framework, tapi secara teknis React bukanlah framework, ia hanya library yang digunakan untuk memudahkan dalam membangun User Interface.

React mengusung konsep *Virtual DOM*. Yang mana ketika proses Developing, react akan menyimpan salinan DOM yang asli terlebih dahulu. Baru ketika proses build sudah dijalankan React akan menampilkan hasil Real DOM nya.

Dengan VDOM ketika developing, React akan menjalankan proses diffing pada setiap perubahan. Kemudian mengindentifikasi perubahan dan menggenerate ke Real DOM. VDOM sangat membantu karena membuat rendering aplikasi lebih cepat dan meningkatkan performa.

<br><br>

# Create React App (Instalation)

Ketikan perintah dibawah ini kedalam terminal

```shell
    # clone react app in your project
    npx create-react-app project-name

    # masuk ke folder project-name
    cd project-name

    # jalankan npm
    npm start
```

```shell
    # Install globally create-react-app
    npm install -g create-react-app

    # clone react-app from global
    create-react-app project-name

    # masuk ke folder project-name
    cd project-name

    # jalankan npm
    npm start
```

<br><br>

# Hello World

Pertama, Buka file App.js yang berada didalam folder src.

Kemudian, isikan script dibawah ini :

```jsx
    import './App.css';

    function App() {
        return(
            <p> Hello World!!! </p>
        )
    }

    export default App;
```

Selanjutnya, jalankan `npm start` dan lihat hasilnya.

![Result Hello World](./src/hello_world.png)

<br><br>

# Struktur Direktori pada Project React

```shell
    |- project-name                     
        |- node_modules             # berisi paket nodejs
        |
        |- public                   # berisi css, image, icon, html dll
            |- index.html           # main html yang digunakan react untuk render component
        |
        |- src                      # berisi kode aplikasi react
            |- App.js               # inti component 
            |- App.test.js          # testing component react
            |- index.js             # berisi kode untuk render component ke real dom
            |- serviceWorker.js     # Service Worker for PWA
            |- setTests.js          # kode untuk testing aplikasi
```

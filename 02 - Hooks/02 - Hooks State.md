# Mengenal Hooks

```jsx
import React, { useState } from 'react';

function Example() {
  // Deklarasi sebuah variabel state baru, dimana akan dinamakan "count"
  const [count, setCount] = useState(0);

  return (
    <div>
      <p>Anda klik sebanyak {count} kali</p>
      <button onClick={() => setCount(count + 1)}>
        Klik saya
      </button>
    </div>
  );
}
```

`useState` merupakan bagian dari Hooks yang fungsinya untuk menambahkan state lokal didalam function component. `useState` akan berisi 2 variabel, yaitu variabel state saat ini (*count*) dan function untuk memperbarui nilai state (*setCount*). `useState` memiliki argument yang digunakan sebagai state awal. Pada contoh diatas berarti nilai awal `count` adalah `0`.



Seperti yang anda lihat diatas, biasanya state hanya tersedia di Class Component. Tapi berkat bantuan hooks, state dapat tersedia di function component. 

Jika kita tulis contoh diatas kedalam bentuk Class Component akan menjadi :

```jsx
class Example extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0
    };
  }

  render() {
    return (
      <div>
        <p>Anda menekan sebanyak {this.state.count} kali</p>
        <button onClick={() => this.setState({ count: this.state.count + 1 })}>
          Klik saya
        </button>
      </div>
    );
  }
}
```

# Kapan Hooks digunakan?

Hooks digunakan ketika anda menulis sebuah function component dan ternyata Function Component tersebut membutuhkan beberapa state di dalamnya. Daripada mengubahnya kedalam bentuk class, lebih baik menggunakan Hooks di dalam function component yang sudah ada.


# Mendeklarasikan lebih dari 1 state

Didalam function Component kita dapat mendeklarasikan lebih dari 1 state. Example :

```jsx
function ContohDenganBanyakState() {
  // Deklarasi banyak variabel state!
  const [age, setAge] = useState(42);
  const [fruit, setFruit] = useState('pisang');
  const [todos, setTodos] = useState([{ text: 'Belajar Hooks' }]);
  // ...
}
```

# Memanggil State

Seperti pada contoh program [diatas](#mengenal-hooks), pemanggilan state pada hook berbeda dengan state pada class. Jika di class wajib menggunakan keyword `this` maka di hook tidak, karena state di hook didefinisikan sebagai variabel function.
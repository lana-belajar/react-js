# Apa itu Hooks Effect?

Hook ini memiliki fungsi yang sama dengan *componentDidMount*, *componentDidUpdate*, dan *componentWillUnmount* pada kelas React, tetapi disatukan menjadi satu API. Dengan menggunakan perintah `useEffect`.  `useEffect` akan berjalan setelah pertama kali render (*componentDidMount*) dan setiap setelah pembaruan (*componentDidUpdate*).

`useEffect` biasanya digunakan untuk melakukan pengambilan data, berlangganan data (subscription), atau mengubah DOM dari komponen sebelumnya secara manual. Hal tersebut biasa disebut dengan *side effect*.

Efek dideklarasikan di dalam komponen sehingga mereka akan mendapat akses pada prop dan state komponen tersebut. Secara bawaan, React menjalankan efek setelah setiap render (termasuk pada render pertama).

Example :

```jsx
import React, { useState, useEffect } from 'react';

function Example() {
  const [count, setCount] = useState(0);

  // Sama seperti componentDidMount dan componentDidUpdate:
  useEffect(() => {
    // Memperbarui judul dokumen menggunakan API browser
    document.title = `Anda klik sebanyak ${count} kali`;
  });

  return (
    <div>
      <p>Anda klik sebanyak {count} kali</p>
      <button onClick={() => setCount(count + 1)}>
        Klik saya
      </button>
    </div>
  );
}
```

# Perbedaan useEffect dengan State Effect Class Component

Component pada contoh [diatas](#apa-itu-hooks-effect) jika ditulis dalam bentuk class jadi :

```jsx
class Example extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0
    };
  }

  componentDidMount() {
    document.title = `You clicked ${this.state.count} times`;
  }
  componentDidUpdate() {
    document.title = `You clicked ${this.state.count} times`;
  }

  render() {
    return (
      <div>
        <p>You clicked {this.state.count} times</p>
        <button onClick={() => this.setState({ count: this.state.count + 1 })}>
          Click me
        </button>
      </div>
    );
  }
}
```



# Side Effect yang memerlukan pembersihn

Terdapat 2 macam *side effect* didalam React :
1. Tidak membutuhkan pembersihan
2. Membutuhkan pembersihan

Untuk contoh side effect yang tidak membutuhkan pembersihan sudah kita bahas diatas [diatas](#apa-itu-hooks-effect). Sekarang kita akan membahas side effect yang memerlukan pembersihan.

Beberapa side effect terkadang membutuhkan suatu pembersihan agar tidak menyebabkan kebocoran memori. Misalnya pada *subcription*.

Berikut contoh pembersihan (*unsubcribe*) pada Class Component :

```jsx
class FriendStatus extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isOnline: null };
    this.handleStatusChange = this.handleStatusChange.bind(this);
  }

  // Ketika component dibuat. ChatAPI akan melakukan subscribe
  // pada component ini
  componentDidMount() {
    // Katakanlah module ChatAPI itu ada :v 
    ChatAPI.subscribeToFriendStatus(
      this.props.friend.id,
      this.handleStatusChange
    );
  }

  // Ketika component selesai dibuat. ChatAPI akan unsubscribe
  // component ini
  componentWillUnmount() {
    ChatAPI.unsubscribeFromFriendStatus(
      this.props.friend.id,
      this.handleStatusChange
    );
  }

  handleStatusChange(status) {
    this.setState({
      isOnline: status.isOnline
    });

  }
  render() {
    if (this.state.isOnline === null) {
      return 'Loading...';
    }
    return this.state.isOnline ? 'Online' : 'Offline';
  }
}
```

Berikut contoh pembersihan (*unsubcribe*) pada Function Component (Hooks):

```jsx
import React, { useState, useEffect } from 'react';

function FriendStatus(props) {
  const [isOnline, setIsOnline] = useState(null);

  useEffect(() => {
    // Membuat fungsi handle Status
    function handleStatusChange(status) {
      setIsOnline(status.isOnline);
    }
    // Subscribe to Component
    ChatAPI.subscribeToFriendStatus(props.friend.id, handleStatusChange);
    // useEffect return function cleanup untuk digunakan sebagai unsubscribe
    return function cleanup() {
      ChatAPI.unsubscribeFromFriendStatus(props.friend.id, handleStatusChange);
    };
  });

  if (isOnline === null) {
    return 'Loading...';
  }
  return isOnline ? 'Online' : 'Offline';
}
```

Pada contoh diatas `useEffect` mengembalikan nilai berupa function yang berisi pembersihan effect. Setiap effect dapat mengembalikan sebuah function untuk pembersihan. Halt tersebut bertujuan agar tetap menjaga logika untuk menambah dan menghapus sebuah subscriptions.

React menjalankan pembersihan ketika komponen dilepaskan (*componentWillUnmount*). Effects akan berjalan setiap terjadi render. Oleh sebab itulah diperlukan cleanup effect dari sisa render sebelumnya. Tujuannya untuk meminimalisir bug, menghindari penumpukan memori, dan meningkatkan peforma aplikasi.


# Tips

## Gunakan banyak effect untuk memisahkan masalah

Example :

```jsx
function FriendStatusWithCounter(props) {
  const [count, setCount] = useState(0);
  // Effect 1 (Update titles)
  useEffect(() => {
    document.title = `You clicked ${count} times`;
  });

  // Effect 2 (Get status from API) need cleanup
  const [isOnline, setIsOnline] = useState(null);
  useEffect(() => {
    function handleStatusChange(status) {
      setIsOnline(status.isOnline);
    }

    ChatAPI.subscribeToFriendStatus(props.friend.id, handleStatusChange);
    // cleanup function
    return () => {
      ChatAPI.unsubscribeFromFriendStatus(props.friend.id, handleStatusChange);
    };
  });
  // ...
}
```

Jika ditulis dalam bentuk class menjadi :

```jsx
class FriendStatusWithCounter extends React.Component {
  constructor(props) {
    super(props);
    this.state = { count: 0, isOnline: null };
    this.handleStatusChange = this.handleStatusChange.bind(this);
  }

  componentDidMount() {
    // Effect 1
    document.title = `You clicked ${this.state.count} times`;
    // Effect 2
    ChatAPI.subscribeToFriendStatus(
      this.props.friend.id,
      this.handleStatusChange
    );
  }

  componentDidUpdate() {
    document.title = `You clicked ${this.state.count} times`;

    // Always update friend status (Untuk menghindari bug aplikasi)
    // Unsubscribe from the previous friend.id
    ChatAPI.unsubscribeFromFriendStatus(
      prevProps.friend.id,
      this.handleStatusChange
    );
    // Subscribe to the next friend.id
    ChatAPI.subscribeToFriendStatus(
      this.props.friend.id,
      this.handleStatusChange
    );
  }

  componentWillUnmount() {
    ChatAPI.unsubscribeFromFriendStatus(
      this.props.friend.id,
      this.handleStatusChange
    );
  }

  handleStatusChange(status) {
    this.setState({
      isOnline: status.isOnline
    });
  }
  // ...
```

## Mengoptimalkan perfroma aplikasi dengan membuat effect hanya berjalan ketika dibutuhkan

Misalnya tambahkan logic ketika update :

```jsx
componentDidUpdate(prevProps, prevState) {
  if (prevState.count !== this.state.count) {
    document.title = `You clicked ${this.state.count} times`;
  }
}
```

Atau hooknya :

```jsx
useEffect(() => {
  document.title = `You clicked ${count} times`;
}, [count]); // Only re-run the effect if count changes

useEffect(() => {
  function handleStatusChange(status) {
    setIsOnline(status.isOnline);
  }

  ChatAPI.subscribeToFriendStatus(props.friend.id, handleStatusChange);
  return () => {
    ChatAPI.unsubscribeFromFriendStatus(props.friend.id, handleStatusChange);
  };
}, [props.friend.id]); // Only re-subscribe if props.friend.id changes
```
Sekarang muncul masalah baru. Bagaimana cara menggunakan hook yang isinya sama sacara berkali kali?

# Hook Custom

Sebuah Hook kustom adalah sebuah fungsi Javascript yang namanya dimulai dengan ”use” dan mungkin memanggil Hooks lain. Misalnya, useFriendStatus dll. Contoh penggunaan hook custom :

```jsx
import { useState, useEffect } from 'react';

function useFriendStatus(friendID) {
  const [isOnline, setIsOnline] = useState(null);

  useEffect(() => {
    function handleStatusChange(status) {
      setIsOnline(status.isOnline);
    }

    ChatAPI.subscribeToFriendStatus(friendID, handleStatusChange);
    return () => {
      ChatAPI.unsubscribeFromFriendStatus(friendID, handleStatusChange);
    };
  });

  return isOnline;
}

function FriendStatus(props) {
  const isOnline = useFriendStatus(props.friend.id);

  if (isOnline === null) {
    return 'Loading...';
  }
  return isOnline ? 'Online' : 'Offline';
}

function FriendListItem(props) {
  const isOnline = useFriendStatus(props.friend.id);

  return (
    <li style={{ color: isOnline ? 'green' : 'black' }}>
      {props.friend.name}
    </li>
  );
}
```

Kita bisa menggunakan `useFriendStatus` berkali kali tanpa adanya bentrok data pada `useFriendStatus` karena semua state dan efek sepenuhnya terisolasi.

# Membuat Reducer

Misalnya, mungkin Anda memiliki komponen kompleks yang berisi banyak status lokal yang dikelola secara ad-hoc. useState tidak membuat pemusatan logika pembaruan menjadi lebih mudah sehingga Anda mungkin memilih untuk menuliskannya sebagai reducer seperti di Redux.

1. Buat Reducer Contorller

```jsx
function todosReducer(state, action) {
  switch (action.type) {
    case 'add':
      return [...state, {
        text: action.text,
        completed: false
      }];
    // ... other actions ...
    default:
      return state;
  }
}
```

2. Buat Reducer Action

```jsx
function useReducer(reducer, initialState) {
  const [state, setState] = useState(initialState);

  function dispatch(action) {
    const nextState = reducer(state, action);
    setState(nextState);
  }

  return [state, dispatch];
}
```

3. Gunakan Reducer didalam component

```jsx
function Todos() {
  const [todos, dispatch] = useReducer(todosReducer, []);

  function handleAddClick(text) {
    dispatch({ type: 'add', text });
  }

  // ...
}
```

